import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

Page {
    id: importPage

    property var activeTransfer
    property bool allowMultipleFiles

    signal accept(var files)
    signal close()

    header: PageHeader {
        title: i18n.tr("Import a file from")

        leadingActionBar.actions: Action {
            iconName: "back"
            text: i18n.tr("Back")

            onTriggered: close();
        }
    }

    Rectangle {
        anchors.fill: parent
        anchors.topMargin: importPage.header.height

        ContentTransferHint {
            anchors.fill: parent
            activeTransfer: importPage.activeTransfer
        }

        ContentPeerPicker {
            visible: importPage.visible
            showTitle: false
            contentType: ContentType.All
            handler: ContentHandler.Source

            onPeerSelected: {
                peer.selectionType = importPage.allowMultipleFiles
                    ? ContentTransfer.Multiple
                    : ContentTransfer.Single;

                importPage.activeTransfer = peer.request();
                stateChangeConnection.target = importPage.activeTransfer;
            }

            onCancelPressed: {
                close();
            }
        }
    }

    Connections {
        id: stateChangeConnection
        target: null

        onStateChanged: {
            var statesForDebug = ["Created","Initiated","InProgress","Charged","Collected","Aborted","Finalized","Downloading","Downloaded"]
            console.log("DEBUG: activeTrasfer State is", statesForDebug[activeTransfer.state])

            switch (activeTransfer.state) {
                case ContentTransfer.Aborted:
                  console.log("Aborted")
                case ContentTransfer.Collected:
                  console.log("Finalize and close")
                  //Finalize should clean ContentHub temporary files
                  activeTransfer.finalize();
                  close();
                  break;
                case ContentTransfer.Finalized:
                  console.log("Finalized")
                  break;
                case ContentTransfer.InProgress:
                  console.log("InProgress")
                  break;
                case ContentTransfer.Charged:
                  var selectedItems = []

                  for (var i in importPage.activeTransfer.items) {
                    /* Imported files can be moved calling the method move()
                     *   importPage.activeTransfer.items[i].move(directory, optionalFileName)
                     *
                     *   https://api-docs.ubports.com/sdk/apps/qml/Ubuntu.Content/ContentItem.html#sdk-ubuntu-content-contentitem-move
                     */

                    var base64String = String(importPage.activeTransfer.items[i].toDataURI());
                    var fileMimeType = base64String.slice(base64String.indexOf(":") + 1, base64String.indexOf(";"));
                    console.log("MimeType",fileMimeType);

                    selectedItems.push(
                        importPage.activeTransfer.items[i].url
                    );
                  }

                  accept(selectedItems);
                  break;
                default:
                  console.log("Other state?",activeTransfer.state)
            }
        }
    }
}
